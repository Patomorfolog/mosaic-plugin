(function ($, window, document, undefined) {

  'use strict';

$(function(){

	$('.mosaic-names-list__item>a').hover(function(){

// MOUSE OVER -->
		
		var elementOrder = $(this).parent('li').index();
		var elementToExpand = $('.mosaic__element').eq(elementOrder);
		
		elementToExpand.children('img').not('.--cloned').clone().appendTo(elementToExpand);

		var imgToExpand = elementToExpand.children('img');
		var positionMarker = imgToExpand.first().position();
		
		imgToExpand.addClass('--cloned');

		imgToExpand.last().addClass('--to-expand').css({
			'top': positionMarker.top,
			'left': positionMarker.left
		}).addClass('--expanded').hide();

		$('.mosaic--overlay').show();

		var findAnimations = $('.mosaic').find('img');

		findAnimations.promise().done(function() {

				elementToExpand.children('.--expanded').show().addClass('drop-shadow').animate({
			  left: '1px',
	      top: '1px',
	      right: '0px',
	      bottom: '0px',
	      width: '438px',
	      height: '438px'
			}, 350, 'linear');	

		});
	},

// MOUSE OUT -->

	function(){

		var elementOrder = $(this).parent('li').index();
		var elementToExpand = $('.mosaic__element').eq(elementOrder);

		var initialPosition = elementToExpand.children().first().position();
/*
		$('.--expanded').animate({
		  left: initialPosition.left,
      top: initialPosition.top,
      width: '108px',
      height: '108px'
		}, 20, 'linear', function(){
			
			var cleanUp = $(this).parent();

			cleanUp.children('img').removeClass('--cloned --to-expand --expanded');
			cleanUp.children('img:nth-child(2)').remove();

		});		
*/
		$('.--expanded').hide('blind', 200, function(){

			var cleanUp = $(this).parent();

			cleanUp.children('img').removeClass('--cloned --to-expand --expanded');
			cleanUp.children('img:nth-child(2)').remove();

		});

			$('.mosaic--overlay').hide();

	});
});

})(jQuery, window, document);